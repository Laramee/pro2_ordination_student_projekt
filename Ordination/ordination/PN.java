package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;

public class PN extends Ordination{

	private double antalEnheder;
	private ArrayList<LocalDate> dates = new ArrayList<>();

	public PN(LocalDate startDato, LocalDate slutDato, double antalEnheder) {
		super(startDato, slutDato);
		if(antalEnheder < 0)
		{
			throw new IllegalArgumentException("Enheder skal være positivt"); 
		}
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen
	 * Returnerer true hvis givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if (givesDen.isAfter(getStartDen().minusDays(1)) &&
				givesDen.isBefore(getSlutDen().plusDays(1)))
		{
			dates.add(givesDen);
			return true;
		}
		return false;
	}

	public double doegnDosis() {
		Collections.sort(dates);
		int daysBetween = 1;
		if(dates.size() > 0)
		{
			daysBetween = (int) ChronoUnit.DAYS.between(dates.get(0),dates.get(dates.size()-1)) + 1;
		}
		double samlet = samletDosis();
		return samlet/daysBetween;
	}

	@Override
	public String getType() {
		return "PN";
	}

	public double samletDosis() {
		if(dates.size() == 0)
		{
			return 0;
		}
		
		return dates.size()*antalEnheder;
	}	

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * @return
	 */
	public int getAntalGangeGivet() {
		return dates.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}
	
}
