package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {
	private LocalTime tid;
	private double antal;
	private Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startDato, LocalDate slutDato, double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		super(startDato, slutDato);
		if(morgenAntal < 0 || middagAntal < 0 || aftenAntal < 0 || natAntal < 0)
		{ 
			throw new IllegalArgumentException("Enheder skal være positivt"); 
		}

		doser[0] = new Dosis(LocalTime.of(8, 00), morgenAntal);
		doser[1] = new Dosis(LocalTime.of(12, 00), middagAntal);
		doser[2] = new Dosis(LocalTime.of(18, 00), aftenAntal);
		doser[3] = new Dosis(LocalTime.of(22, 00), natAntal);
		
	}

	public LocalTime getTid() {
		return tid;
	}

	public void setTid(LocalTime tid) {
		this.tid = tid;
	}

	public double getAntal() {
		return antal;
	}

	public void setAntal(double antal) {
		this.antal = antal;
	}
	
	public Dosis[] getDoser()
	{
		return doser;
	}

	@Override
	public double samletDosis() {
		
		return this.doegnDosis() * super.antalDage();
	}

	@Override
	public double doegnDosis() {
		int samletDosis = 0;
		for (int k = 0; k < doser.length; k++) {
			samletDosis += doser[k].getAntal();
		}
		return samletDosis;
	}

	@Override
	public String getType() {

		return "DagligFast";
	}
}
