package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	
    private ArrayList<Dosis> doser = new ArrayList<>();


    public DagligSkaev(LocalDate startDato, LocalDate slutDato)
    {
    	super(startDato, slutDato);
    }
    
    public void opretDosis(LocalTime tid, double antal)
    {
    	Dosis dosis = new Dosis(tid, antal);
    	doser.add(dosis);
    }
  
    public ArrayList<Dosis> getDoser()
    {
    	return new ArrayList<Dosis>(doser);
    }
    
    public void addDosis(Dosis dosis)
    {
    	doser.add(dosis);
    }
    
    public void removeDosis(Dosis dosis)
    {
    	doser.remove(dosis);
    }

	@Override
	public double samletDosis() {
		return doegnDosis() * super.antalDage();
	}

	@Override
	public double doegnDosis() {
		double antal = 0;
		for(int i = 0; i < doser.size(); i++)
		{
			antal += doser.get(i).getAntal();
		}
		return antal;
	}

	@Override
	public String getType() {
		return "Daglig Skaev";
	}
}
