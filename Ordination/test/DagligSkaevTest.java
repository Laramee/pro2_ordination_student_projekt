package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligSkaevTest {
	private DagligSkaev ds;
	private DagligSkaev ds2;
	private DagligSkaev ds3;
	private Laegemiddel lm1;
	private Patient p1;
	
	
	@Before
	public void setUp()
	{
		LocalTime[] tider = {LocalTime.of(8, 00)};
		LocalTime[] tider2 = {LocalTime.of(8, 00), LocalTime.of(12, 00), LocalTime.of(00, 00)};
		
		double[] enheder = {5};
		double[] enheder2 = {5, 10, 5};
		
		p1 = new Patient("1010101010", "Simona", 100);
		lm1 = new Laegemiddel("Aids", 2, 3, 4, "10");
		ds = service.Service.getService().opretDagligSkaevOrdination(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 03), p1, lm1, tider, enheder);
		ds2 = service.Service.getService().opretDagligSkaevOrdination(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 01), p1, lm1, tider2, enheder2);
		ds3 = service.Service.getService().opretDagligSkaevOrdination(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 02), p1, lm1, tider2, enheder2);
	}
	
	@Test
	public void testSamletDosisFlereDage() {
		assertEquals(15, ds.samletDosis(),0.0);
	}
	
	@Test
	public void testSamletDosisEnDag() {
		assertEquals(20, ds2.samletDosis(),0.0);
	}
	
	@Test
	public void testSamletDosisToDage() {
		assertEquals(40, ds3.samletDosis(),0.0);
	}

	@Test
	public void testDoegnDosisFlereDage() {
		assertEquals(5, ds.doegnDosis(), 0.0);
	}
	
	@Test
	public void testDoegnDosisEnDag() {
		assertEquals(20, ds2.doegnDosis(),0.0);
	}

}
