package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligFastTest {


	private DagligFast df;
	private DagligFast df2;

	@Before
	public void setUp()
	{
		Patient p1 = new Patient("1010101010", "Simona", 100);
		Laegemiddel lm1 = new Laegemiddel("Aids", 2, 3, 4, "10");
		df = service.Service.getService().opretDagligFastOrdination(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 03), p1, lm1, 1, 1, 1, 1);
		df2 = service.Service.getService().opretDagligFastOrdination(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 01), p1, lm1, 1, 3, 0, 1);
		
		
	}

	@Test
	public void testSamletDosisFlereDage() {
		assertEquals(12, df.samletDosis(), 0.0);
	}
	
	@Test
	public void testSamletDosisEnDag() {
		assertEquals(5, df2.samletDosis(), 0.0);
	}

	@Test
	public void testDoegnDosisFlereDage() {
		
		assertEquals(4, df.doegnDosis(), 0.0);
	}
	
	@Test
	public void testDoegnDosisEnDag() {
		assertEquals(5, df2.doegnDosis(), 0.0);
	}

}

