package test;

import static org.junit.Assert.*;
import static service.Service.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ordination.*;

public class PNTest {
	
	private PN pn1;
	private Laegemiddel lm1;
	private Patient p1;
	
	
	@Before
	public void setUp()
	{
		p1 = new Patient("1010101010", "Simona", 100);
		
		lm1 = new Laegemiddel("Aids", 2, 3, 4, "10");
		
		pn1 = service.Service.getService().opretPNOrdination(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 03), p1, lm1, 10);
		
	}
	
	
	
	@Test
	public void testSamletDosis() {
		pn1.givDosis(LocalDate.of(2016, 01, 01));
		assertEquals(10, pn1.samletDosis(), 0.0);
		
		pn1.givDosis(LocalDate.of(2016, 01, 02));
		assertEquals(20, pn1.samletDosis(), 0.0);
	}

	@Test
	public void testDoegnDosisEndag() {
		pn1.givDosis(LocalDate.of(2016, 01, 01));
		pn1.givDosis(LocalDate.of(2016, 01, 01));
		assertEquals(20, pn1.doegnDosis(), 0.0);
	}
	
	@Test
	public void testDoegnDosisFlereDage() {
		pn1.givDosis(LocalDate.of(2016, 01, 01));
		pn1.givDosis(LocalDate.of(2016, 01, 02));
		assertEquals(10, pn1.doegnDosis(), 0.0);
		pn1.givDosis(LocalDate.of(2016, 01, 03));
		assertEquals(10, pn1.doegnDosis(), 0.0);
	}

	@Test
	public void testGivDosisFalse()
	{
		assertEquals(false, pn1.givDosis(LocalDate.of(2116, 01, 01)));
		assertEquals(false, pn1.givDosis(LocalDate.of(1916, 01, 01)));
	}
	
	@Test
	public void testGivDosisTrue()
	{
		assertEquals(true, pn1.givDosis(LocalDate.of(2016, 01, 01)));
	}
	
}
