package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sun.xml.internal.ws.wsdl.writer.document.Service;

import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.Patient;
import storage.Storage;


public class ServiceTest {
	
	private static Laegemiddel lm1;
	private static Laegemiddel lm2;

	@BeforeClass 
	public static void setUp() throws Exception {
		
		LocalDate now = LocalDate.now();
		Patient p1 = service.Service.getService().createPatient("1010101010", "Simona", 49);
		Patient p2 = service.Service.getService().createPatient("1010101011", "Flemming", 50);
		Patient p3 = service.Service.getService().createPatient("1010101012", "Ida", 51);
		Patient p4 = service.Service.getService().createPatient("1010101013", "Simon", 99);
		Patient p5 = service.Service.getService().createPatient("1010101015", "Ib", 100);
		Patient p6 = service.Service.getService().createPatient("1010101019", "Ahmed", 101);
		Patient p7 = service.Service.getService().createPatient("1010101017", "Bo", 100);
		
		lm1 = new Laegemiddel("Aids", 2, 3, 4, "10");
		lm2 = new Laegemiddel("Notaids", 2, 3, 4, "10");
		
		Ordination o1 = service.Service.getService().opretPNOrdination(now, now, p1, lm1, 1);
		Ordination o2 = service.Service.getService().opretPNOrdination(now, now, p7, lm2, 1);
		p2.addOrdination(o1);
		p3.addOrdination(o1);
		p4.addOrdination(o1);
		p5.addOrdination(o1);
		p6.addOrdination(o1);
	}

	
	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddelNedreVægt()
	{
		assertEquals(2, service.Service.getService().antalOrdinationerPrVægtPrLægemiddel(50, 90, lm1));
	}
	
	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddelØvreVægt()
	{
		assertEquals(2, service.Service.getService().antalOrdinationerPrVægtPrLægemiddel(60, 100, lm1));
	}
	
	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddelAndetLægemiddel()
	{
		assertEquals(1, service.Service.getService().antalOrdinationerPrVægtPrLægemiddel(50, 100, lm2));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testStartFørSlutSkaev() {
		LocalTime[] tid = {LocalTime.now()};
		double[] enhed = {1};
		Patient p1 = new Patient("111111111", "Man", 60);
		service.Service.getService().opretDagligSkaevOrdination(LocalDate.now(), LocalDate.now().minusDays(1), p1, lm1, tid, enhed);
	}
	
	@Test(expected = NullPointerException.class)
	public void testNullVærdi() {
		service.Service.getService().opretPNOrdination(LocalDate.now(), LocalDate.now(), null, lm1, 10);
		fail("Nullpointer");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNegativt() {
		Patient p1 = new Patient("1212121212", "Monna", 70);
		service.Service.getService().opretPNOrdination(LocalDate.now(), LocalDate.now(), p1, lm1, -10);
		fail("Enheder kan ikke være negativ");
	}
	
	@Test(expected = IllegalArgumentException.class) 
	public void testStartFørSlutPN() {
		Patient p1 = new Patient("1212121212", "Monna", 70);
		service.Service.getService().opretPNOrdination(LocalDate.now(), LocalDate.now().minusDays(1), p1, lm1, 10);
		fail("Slutdato er før startdato");
	}
	
}
